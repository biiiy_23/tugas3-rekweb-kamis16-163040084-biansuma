<h2 style="margin-left: 5%">Tambah Data Obat</h2><br>
<div class="form-inline" style="margin-left: 5%">
	<?= form_open('ECommerce/add'); ?>
		<table class="table">
			<tr>
				<td><b>Id Obat</b></td>
				<td>:</td>
				<td><input type="text" name="id_obat" id="id_obat"></td>
			</tr>
			<tr>
				<td><b>Nama Obat</b></td>
				<td>:</td>
				<td><input type="text" name="nama_obat" id="nama_obat"></td>
			</tr>
			<tr>
				<td><b>Jenis Obat</b></td>
				<td>:</td>
				<td><input type="text" name="jenis_obat" id="jenis_obat"></td>
			</tr>
			<tr>
				<td><b>Stok</b></td>
				<td>:</td>
				<td><input type="number" min="1" max="500" name="stok" id="stok"></td>
			</tr>
			<tr>
				<td><b>Harga</b></td>
				<td>:</td>
				<td><input type="number" name="harga" id="harga"></td>
			</tr>
		</table>
		<button class="btn btn-primary" type="submit">Tambah Data</button>
		<a class="btn btn-xs btn-secondary" href="<?= site_url('ECommerce/goToHalamanAdmin') ?>">Kembali</a>
	</form>
</div>