<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function getUser($data) {
		return $this->db->get_where('user', ['username' => $data['username'], 'password' => $data['password']])->row_array();
	}

}

?>