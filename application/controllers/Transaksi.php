<?php 
	
	require APPPATH .  '/libraries/REST_Controller.php';

	class Transaksi extends REST_Controller {
		public function __construct($config = "rest") {
			parent::__construct($config);
			$this->load->model("Transaction_model", "transaksi");
		}

		public function index_get() {
			$transaksi = $this->transaksi->getTransaction();

			$this->response($transaksi);
		}

		public function index_post() {
			$id = $this->post('id');
			$tanggal = $this->post('tanggal');
			$user = $this->post('user');
			$data = [
				'id_transaksi' => $id,
				'tanggal_transaksi' => $tanggal,
				'id_user' => $user
			];
			$insert = $this->transaksi->insertTransaction($data);

			if ($insert) {
				$this->response(['status' => 'success', 200]);
			} else {
				$this->response(['status' => 'fail', 502]);
			}
			
		}

		public function index_put() {
			$id = $this->put('id');
			$insert = $this->transaksi->updateTransaction($id);

			if ($insert) {
				$this->response(['status' => 'success', 200]);
			} else {
				$this->response(['status' => 'fail', 502]);
			}
			
		}
	}

?>