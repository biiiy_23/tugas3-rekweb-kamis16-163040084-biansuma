<?php 
	
	require APPPATH .  '/libraries/REST_Controller.php';

	class Obat extends REST_Controller {

		public function __construct($config = "rest") {
			parent::__construct($config);
			$this->load->model("Obat_model", "obat");
		}

		public function index_get() {
			$id = $this->get('id_obat');
			$keyword = $this->get('keyword');

			if ($keyword != "") {
				$obat = $this->obat->searchObat($keyword)->result();
			} else if($id == '') {
				$obat = $this->obat->getObat(null)->result();
			} else {
				$obat = $this->obat->getObat($id)->result();
			}

			$this->response($obat);
		}

		public function index_put() {
			$id = $this->put('id_obat');
			$nama = $this->put('nama_obat');
			$jenis = $this->put('jenis_obat');
			$stok = $this->put('stok');
			$harga = $this->put('harga');
			$data = [
				'id_obat' => $id,
				'nama_obat' => $nama,
				'jenis_obat' => $jenis,
				'stok' => $stok,
				'harga' => $harga
			];
			$update = $this->obat->updateObat('obat', $data, 'id_obat', $this->put('id_obat'));

			if ($update) {
				$this->response(['status' => 'success', 200]);
			} else {
				$this->response(['status' => 'fail', 502]);
			}
			
		}

		public function index_post() {
			$id = $this->post('id_obat');
			$nama = $this->post('nama_obat');
			$jenis = $this->post('jenis_obat');
			$stok = $this->post('stok');
			$harga = $this->post('harga');
			$obat = [
				'id_obat' => $id,
				'nama_obat' => $nama,
				'jenis_obat' => $jenis,
				'stok' => $stok,
				'harga' => $harga
			];
			$insert = $this->obat->insertObat($obat);

			if ($insert) {
				$this->response(['status' => 'success', 200]);
			} else {
				$this->response(['status' => 'fail', 502]);
			}
			
		}

		public function index_delete() {
			$id = $this->delete('id_obat');
			$delete = $this->obat->deleteObat('obat', 'id_obat', $id);

			if ($delete) {
				$this->response(['status' => 'success', 201]);
			} else {
				$this->response(['status' => 'fail', 502]);
			}
			
		}

	}

 ?>