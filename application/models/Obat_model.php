<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat_model extends CI_Model {

	public function getObat($id = null){
		$this->db->select('*');
		$this->db->from("obat");

		if ($id == null) {
			$this->db->order_by('id_obat', 'asc');
		} else {
			$this->db->where('id_obat', $id);
		}
		return $this->db->get();
	}

	public function insertObat($obat){
		$this->db->insert('obat', $obat);
		return $this->db->affected_rows();
	}	

	public function updateObat($table, $data, $par, $var) {
		$this->db->update($table, $data, [$par => $var]);
		return $this->db->affected_rows();
	}

	public function deleteObat($table, $par, $var) {
		$this->db->where($par, $var);
		$this->db->delete($table);
		return $this->db->affected_rows();
	}

	public function searchObat($keyword) {
		$this->db->select('*');
		$this->db->from('obat');
		$this->db->like('nama_obat', $keyword);
		$this->db->or_like('jenis_obat', $keyword);
		return $this->db->get();
	}
}

?>
