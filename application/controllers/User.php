<?php 
	
	require APPPATH .  '/libraries/REST_Controller.php';

	class User extends REST_Controller {
		public function __construct($config = "rest") {
			parent::__construct($config);
			$this->load->model("User_model", "user");
		}

		public function index_post() {
			$username = $this->post('username');
			$password = $this->post('password');

			$data = [
				'username' => $username,
				'password' => $password
			];

			if($data == null) {
				$user = $this->user->getUser(null);
			} else {
				$user = $this->user->getUser($data);
			}

			$this->response($user);
		}
	}

?>