<?php 
	
	require APPPATH .  '/libraries/REST_Controller.php';

	class Detail extends REST_Controller {
		public function __construct($config = "rest") {
			parent::__construct($config);
			$this->load->model("Transaction_model", "transaksi");
		}

		public function index_get() {
			$id = $this->get('id');


			if($id == '') {
				$detail = $this->transaksi->getDetailTransaction();
			} else {
				$detail = $this->transaksi->getIdDetailTransaction($id);
			}

			$this->response($detail);
		}

		public function index_post() {
			$idDetail = $this->post('id_detail');
			$idTransaksi = $this->post('id_transaksi');
			$idObat = $this->post('id_obat');
			$detail = [
				'id_detail' => $idDetail,
				'id_transaksi' => $idTransaksi,
				'id_obat' => $idObat
			];
			$insert = $this->transaksi->insertDetailTransaction($detail);

			if ($insert) {
				$this->response(['status' => 'success', 200]);
			} else {
				$this->response(['status' => 'fail', 502]);
			}
			
		}
	}

?>